class SessionsController < ApplicationController
  def create
    user = authenticate(session_params)
    if user
      session[:current_user_id] = user.id
      flash[:success] = 'Log in successful!'
      redirect_to root_url
    else
      flash[:danger] = 'Invalid email/password combination'
      render 'new'
    end
  end

  def destroy
    session.delete(:current_user_id)
    flash[:success] = 'You have successfully logged out.'
    @current_user = nil
    redirect_to root_url
  end

  private

  def session_params
    params.require(:session).permit(:email, :password)
  end

  def authenticate(params)
    user = User.find_by(email: params[:email])
    user&.authenticate(params[:password])
  end
end
