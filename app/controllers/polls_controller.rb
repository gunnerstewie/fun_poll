class PollsController < ApplicationController
  rescue_from ActiveRecord::RecordNotFound do
    redirect_to root_path, alert: 'Poll Not Found!'
  end
  before_action :check_auth
  # before_action, only: [:edit, ]
  def index
    @polls = current_user.polls
  end

  def new
    @poll = Poll.new
  end

  def show
    @poll = Poll.find(params[:id])
    @poll_member = @poll.poll_members.build
  end

  def create
    @poll = current_user.polls.build(poll_params)
    if @poll.save
      flash[:success] = 'Poll has created successfuly!'
      redirect_to poll_path(@poll)
    else
      render 'new'
    end
  end

  def edit
    @poll = Poll.find(params[:id])
  end

  def update
    @poll = Poll.find(params[:id])
    if @poll.update(poll_params)
      flash[:success] = 'Poll was successfuly updated!'
      redirect_to poll_path
    else
      render 'edit'
    end
  end

  def destroy
    Poll.find(params[:id]).destroy
    flash[:success] = 'Poll has been destroyed!'
    redirect_to root_path
  end

  private

  def check_auth
    return if current_user

    flash[:info] = 'Please log in'
    redirect_to login_path
  end

  def poll_params
    params.require(:poll).permit(:title, :description, :start_at, :end_at)
  end
end
