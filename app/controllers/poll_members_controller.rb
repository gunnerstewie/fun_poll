# frozen_string_literal: true

class PollMembersController < ApplicationController
  def create
    user = User.find_by!(email: permitted_params[:email])
    poll = Poll.find(permitted_params[:poll_id])

    user.invite_polls << poll

    flash[:success] = 'User invited!'
    redirect_to poll_path(poll)
  rescue ActiveRecord::RecordNotFound
    flash[:danger] = 'Invitation error'
    redirect_to polls_path
  end

  private

  def permitted_params
    {
      email: params[:email].strip,
      poll_id: params[:poll_id]
    }
  end
end
