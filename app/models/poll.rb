# frozen_string_literal: true

class Poll < ApplicationRecord
  belongs_to :user
  has_many :options
  has_many :poll_members
  has_many :invited_users, through: :poll_members, source: :user

  validates :title, presence: true
  validates :description, presence: true
  validates :start_at, presence: true
  validates :end_at, presence: true

  accepts_nested_attributes_for :options
end
