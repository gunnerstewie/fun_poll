class Option < ApplicationRecord
  belongs_to :poll
  has_many :poll_members, dependent: :nullify
  validates :title, presence: true
end
