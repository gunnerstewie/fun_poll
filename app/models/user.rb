# frozen_string_literal: true

class User < ApplicationRecord
  has_many :poll_members, dependent: :destroy
  has_many :invite_polls, through: :poll_members, source: :poll
  has_many :polls

  validates :name, presence: true, length: { maximum: 20 }
  validates :email, presence: true, length: { maximum: 40 }, uniqueness: true
  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true

  has_secure_password
end
