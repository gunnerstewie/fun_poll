# frozen_string_literal: true

Rails.application.routes.draw do
  # This place is used for path to pages.
  root 'pages#index'

  get '/index', to: 'pages#index'
  get '/about', to: 'pages#about'
  get '/signup', to: 'users#new'
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  resources :users

  resources :polls
  resources :poll_members
end
