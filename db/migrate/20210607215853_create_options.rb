class CreateOptions < ActiveRecord::Migration[6.1]
  def change
    create_table :options do |t|
      t.string :title, null: false

      t.timestamps
    end
    add_reference :options, :poll, foreign_key: true
    add_reference :poll_members, :option, foreign_key: true
  end
end
