class CreatePollMembers < ActiveRecord::Migration[6.1]
  def change
    create_table :poll_members do |t|
    t.timestamps
    end
    add_reference :poll_members, :user, foreign_key: true
    add_reference :poll_members, :poll, foreign_key: true
  end
end
