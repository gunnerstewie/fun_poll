require 'rails_helper'
RSpec.describe 'Login', type: :request do
  before do
    @user = create(:user)
  end
  it 'logs the user in with correct credentials' do
    post login_path, params: { session: { email: 'alexandr@gmail.com', password: '12345678' } }
    expect(response).to redirect_to root_path
    expect(session[:current_user_id]).to eq @user.id
  end
  it 'renders login form when user not found by email' do
    post login_path, params: { session: { email: 'not@found.com', password: '' } }
    expect(response).to render_template(:new)
    expect(session[:current_user_id]).to be_nil
  end
  it 'renders login form when user password is incorrect' do
    post login_path, params: { session: { email: :email, password: 'wrong' } }
    expect(response).to render_template(:new)
    expect(session[:current_user_id]).to be_nil
  end
end