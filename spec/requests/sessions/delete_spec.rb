require 'rails_helper'
RSpec.describe 'Logout', type: :request do
  before do 
    @user = @current_user
  end

  it "should logout user who was logged in" do
    delete logout_path
    expect(response).to redirect_to root_path
    expect(session[:current_user_id]).to be_nil
  end
end

  
