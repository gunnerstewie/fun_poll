FactoryBot.define do
  factory :poll_member do
    user_id { 1 }
    poll_id { 2 }
    user
    poll
  end
end
