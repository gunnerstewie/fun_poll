FactoryBot.define do
  factory :poll do
    user
    title { 'Amazing Poll' }
    description { 'Some short but useful description' }
    start_at { Date.iso8601 }
    end_at { Date.iso8601 + 1.hours }
  end
end