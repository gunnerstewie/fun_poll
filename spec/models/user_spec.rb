# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  before do
    @user = build(:user)
  end
  it 'validations' do
    @user.should validate_uniqueness_of(:email)
    @user.should validate_length_of(:password).is_at_least(6)
    @user.should validate_length_of(:name).is_at_most(20)
    @user.should validate_length_of(:email).is_at_most(40)
  end
end
