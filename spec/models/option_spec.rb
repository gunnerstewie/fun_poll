require 'rails_helper'

RSpec.describe Option, type: :model do
  context 'validations' do
    it { is_expected.to belong_to(:poll) }
    it { is_expected.to have_many(:poll_members).dependent(:nullify) }
  end
end