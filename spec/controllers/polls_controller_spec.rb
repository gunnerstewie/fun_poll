require 'rails_helper'

RSpec.describe PollsController, type: :controller do
  describe 'if user logged in' do
    let(:current_user) { create(:user) }
    before do
      allow_any_instance_of(described_class).to receive(:current_user) { current_user }
    end
    describe '#index' do
      let(:another_user) { create(:user, email: "ffjfjfjfjoooo@gmail.com") }
      let!(:poll_1) { create(:poll, user: current_user) }
      let!(:poll_2) { create(:poll, user: another_user) }
      it 'shows list of users polls' do
        get :index

        expect(assigns[:polls].first).to eq poll_1
      end
    end
    describe '#show' do
      let!(:poll) { create(:poll, user: current_user) }
      it 'show info about poll' do
        get :show, params: { id: current_user.id }

        expect(response).to render_template(:show)
        expect(assigns[:poll].title).to eq poll.title
        expect(assigns[:poll].id).to eq poll.id
      end
    end
    describe '#edit' do
      let!(:poll) { create(:poll, user: current_user) }
      it 'render form for logged in user' do
        get :edit, params: { id: poll.id }

        expect(response).to render_template(:edit)
        expect(assigns[:poll]).to eq poll
      end
    end
    describe '#new' do
      it 'render form for logged in user' do
        get :new
        expect(response).to render_template(:new)
      end
      it 'confirm when instance variable id is null' do
        get :new
        expect(assigns[:poll]).to eq(Poll.new)
      end
    end
    describe '#create' do
      subject{ post :create, params: { poll: { title: "papapapapa", description: 'Some short but useful description', start_at: Date.iso8601, end_at: Date.iso8601 + 1.hours } } }
      it 'check that poll was created' do
        expect{ subject }.to change { Poll.count }.by(1)
      end
      it 'redirect to list of all polls owned by user' do
        expect(subject).to redirect_to :action => :show, :id => assigns(:poll).id
      end
    end
    describe '#destroy' do
      let!(:poll) { create(:poll, user: current_user) }
      subject { delete :destroy, params: { id: poll.id } }
      it 'check that poll was deleted' do
        expect { subject }.to change{ Poll.count }.by(-1)
      end
    end
    describe '#update' do
      let!(:poll) { create(:poll, user: current_user) }
      subject { put :update, params: { id: poll.id, poll: { title: "some changed poll", description: "This is changed poll", start_at: Date.iso8601 + 1.hours, end_at: Date.iso8601 + 2.hours } } }
      before { subject }
      it 'check that poll data is updated' do
        expect(poll.reload.title).to eq 'some changed poll'
      end
      it 'redirect to list of all polls owned by user' do
        expect(response).to redirect_to :action => :show, :id => assigns(:poll).id
      end
    end
  end
end