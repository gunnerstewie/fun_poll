require 'rails_helper'

RSpec.describe PollMembersController, type: :controller do
  describe 'if user  was created poll' do
    let(:current_user) { create(:user) }
    before do
      allow_any_instance_of(described_class).to receive(:current_user) { current_user }
    end
    let!(:invited_user) { create(:user, email: 'example@gmail.com') }
    let!(:poll) { create(:poll) }
    describe 'invite user to poll' do
      subject { post :create, params: { email: 'example@gmail.com', poll_id: 1 } }
      it 'check that poll was created' do
        expect { subject }.to change { PollMember.count }.by(1)
      end
    end
  end
end